# Atlassian documentation #

The documentation in this repository is for products that are no longer supported by Atlassian. This documentation is not maintained by Atlassian, but is open source and can be updated by anyone who wants to help improve it. 

## Read the documentation ##
[Atlassian documentation](http://atlassian-docs.bitbucket.org/)